/*
  Copyright (c) 2016  ESRF

  Permission is hereby granted, free of charge, to any person
  obtaining a copy of this software and associated documentation
  files (the "Software"), to deal in the Software without
  restriction, including without limitation the rights to use,
  copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the
  Software is furnished to do so, subject to the following
  conditions:
  
  The above copyright notice and this permission notice shall be
  included in all copies or substantial portions of the Software.
  
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
  OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
  NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
  HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
  WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
  FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
  OTHER DEALINGS IN THE SOFTWARE.

*/

#include<stdio.h>
#include<math.h>
#include<string.h>


#define swap(a,b) tmp=a;a=b; b=tmp;



unsigned next_reverse(unsigned i,unsigned d) {
  unsigned res;
  unsigned m = i, mn = d;
  while(i>=d && d>=1) {
    i  -= d;
    d   = d/2;
  }
  i = i+d;
  return i;
}


void fourier1D(double* data, unsigned n, int direction)
{
  
  double cprodtmpr; 
#define COMPEXP( a,b, omega   )    a = cos( omega  ) ; b=sin(omega);
#define CSUM( rr,ri,ar,ai,br,bi  )  rr = ar+br; ri=ai+bi;
#define CPROD( rr,ri,ar,ai,br,bi  )  cprodtmpr = ar*br-ai*bi; ri = ar*bi+ai*br; rr=cprodtmpr;
  
  double tmp;
  unsigned reversed = n/2 ;

  double ar,ai,anr, ani,tmpr,tmpi;
  unsigned  isep=1;// isep = iseparation
  unsigned i,j, begin;


  for (  i=1; i<n; i++) {
    if( i<reversed) {
      
      swap(data[2*i], data[2*reversed]);
      swap(data[2*i+1], data[2*reversed+1]);
      
    }
    reversed = next_reverse(reversed,n/2);
  }
  while (n>isep) {
    
    COMPEXP(ar,ai, -direction*(M_PI/isep)  );
    COMPEXP(anr,ani,0.0);
    
    for ( begin=0; begin < isep; begin += 1) {
      for (i=begin,  j=begin+isep; i <n ; i += isep*2 ,  j += isep*2) {
	CPROD(tmpr, tmpi, anr,  ani, data[2*j],data[2*j+1]      ) ;
	CSUM( data[2*j],  data[2*j+1],  data[2*i],  data[2*i+1],   -tmpr, -tmpi );
	CSUM( data[2*i],  data[2*i+1],  data[2*i],  data[2*i+1],  +tmpr,+tmpi );
      }
      CPROD( anr,ani, ar,ai, anr,ani  ) ;
    }
    isep *= 2;
  }
#undef COMPEXP
#undef CSUM
#undef CPROD
}


void nd_fourier1D_V(double* data,int dim0, unsigned ndimfft, int stride0, int stridefft,  int direction)
{
  
  double cprodtmpr; 
#define COMPEXP( a,b, omega   )    a = cos( omega  ) ; b=sin(omega);
#define CSUM( rr,ri,ar,ai,br,bi  )  rr = ar+br; ri=ai+bi;
#define CPROD( rr,ri,ar,ai,br,bi  )  cprodtmpr = ar*br-ai*bi; ri = ar*bi+ai*br; rr=cprodtmpr;
  
  unsigned n = ndimfft; 

  double tmp;
  unsigned reversed = n/2 ;
  
  double ar,ai,anr, ani,tmpr,tmpi;
  unsigned  isep=1;// isep = iseparation
  unsigned i,j, begin,i0;
  
  for ( i=1; i<n; i++) {
    if( i<reversed) {
      for( i0=0; i0<dim0; i0++) { 
	swap(data[i0*stride0+ i*stridefft    ], data[i0*stride0 +reversed *stridefft  ]);
	swap(data[i0*stride0+ i*stridefft +1], data[i0*stride0+reversed*stridefft +1]);
      }
    }
    reversed = next_reverse(reversed,n/2);
  }
  
  
  while (n>isep) {
    COMPEXP(ar,ai, -direction*(M_PI/isep)  );
    COMPEXP(anr,ani,0.0);
    for ( begin=0; begin < isep; begin += 1) {
      for (i=begin,  j=begin+isep; i <n ; i += isep*2 ,  j += isep*2) {
	for( i0=0; i0<dim0; i0++) { 
	  CPROD(tmpr, tmpi, anr,  ani, data[i0*stride0+j*stridefft],data[i0*stride0+j*stridefft+1]      ) ;
	  CSUM( data[i0*stride0+j*stridefft],  data[i0*stride0+j*stridefft+1],  data[i0*stride0+i*stridefft],  data[i0*stride0+i*stridefft+1],   -tmpr, -tmpi );
	  CSUM( data[i0*stride0+i*stridefft],  data[i0*stride0+i*stridefft+1],  data[i0*stride0+i*stridefft],  data[i0*stride0+i*stridefft+1],  +tmpr,+tmpi );
	}
      }
      CPROD( anr,ani, ar,ai, anr,ani  ) ;
    }
    isep *= 2;
  }
#undef COMPEXP
#undef CSUM
#undef CPROD

}



void nd_fourier1D(double* data,int dim0, unsigned ndimfft, int stride0, int stridefft,  int  direction)
{
  
  double cprodtmpr; 
#define COMPEXP( a,b, omega   )    a = cos( omega  ) ; b=sin(omega);
#define CSUM( rr,ri,ar,ai,br,bi  )  rr = ar+br; ri=ai+bi;
#define CPROD( rr,ri,ar,ai,br,bi  )  cprodtmpr = ar*br-ai*bi; ri = ar*bi+ai*br; rr=cprodtmpr;
  
  unsigned n = ndimfft; 

  double tmp;
  unsigned  reversed;
  double ar,ai,anr, ani,tmpr,tmpi;
  unsigned  isep ;// isep = iseparation
  unsigned i,j, begin, i0;
  
  for( i0=0; i0<dim0; i0++) { 
    reversed = n/2 ;
    for ( i=1; i<n; i++) {
      if( i<reversed) {
	swap(data[i0*stride0+ i*stridefft    ], data[i0*stride0 +reversed *stridefft  ]);
	swap(data[i0*stride0+i*stridefft +1], data[i0*stride0+reversed*stridefft +1]);
      }
      reversed = next_reverse(reversed,n/2);
    }
  }
  
  for( i0=0; i0<dim0; i0++) { 
    isep = 1;
    while (n>isep) {
      COMPEXP(ar,ai, -direction*(M_PI/isep)  );
      COMPEXP(anr,ani,0.0);
      for ( begin=0; begin < isep; begin += 1) {
	for (i=begin,  j=begin+isep; i <n ; i += isep*2 ,  j += isep*2) {
	  CPROD(tmpr, tmpi, anr,  ani, data[i0*stride0+j*stridefft],data[i0*stride0+j*stridefft+1]      ) ;
	  CSUM( data[i0*stride0+j*stridefft],  data[i0*stride0+j*stridefft+1],  data[i0*stride0+i*stridefft],  data[i0*stride0+i*stridefft+1],   -tmpr, -tmpi );
	  CSUM( data[i0*stride0+i*stridefft],  data[i0*stride0+i*stridefft+1],  data[i0*stride0+i*stridefft],  data[i0*stride0+i*stridefft+1],  +tmpr,+tmpi );
	}
	CPROD( anr,ani, ar,ai, anr,ani  ) ;
      }
      isep *= 2;
    }
  }
#undef COMPEXP
#undef CSUM
#undef CPROD

}





main() {
  int N=16;
  int k;
  double data[N*2];   // to test fourier
  double data2D[2* N*2]; // to test nd_fourier1D which should be optimal for line by line transform
  double data2D_V[2* N*2];   // to test  to test nd_fourier1D_V which should be optimal for column by column  transform


  int i;
  for( i=0; i<N; i++) {
    data[2*i]   = cos( i*2*M_PI/N);
    data[2*i+1] = sin( i*2*M_PI/N) ; 
  }
 
  memcpy( data2D, data, N*2*sizeof(double));  // first line
  for(i=0; i<N ; i++)  data2D[2*N+2*i]   = data2D[   2*i] ;  // second line : the conjugate
  for(i=0; i<N ; i++)  data2D[2*N+2*i+1] = -data2D[   2*i +1] ;

  
  for( i=0; i<N; i++) {
    data2D_V[(2*i)*2]   = cos( i*2*M_PI/N);  // first column
    data2D_V[(2*i)*2+1] = sin( i*2*M_PI/N) ;
 
    data2D_V[(2*i)*2 +2   ]   = cos( 3*i*2*M_PI/N);  // second column at higher frequency
    data2D_V[(2*i)*2 +2 +1]   = sin( 3*i*2*M_PI/N) ; 
  }




  // TEST OF PLAIN FOURIER
  fourier1D(data, N , 1);
  for( i=0; i<N*2; i++) {
    printf("%f ", data[i]);
  }
  printf("\n///////////////////////////////////\n");


  // TEST OF FOURIER V OR H( CHANGE ROUTINE NAME ) WITH TWO MATRIX LAYOUTS
  // you can swap the call  to nd_fourier1D with a call to nd_fourier1D_V without changing anything else
  // nd_fourier1D_V might in principle be better for vertical layout

  // TESTIN HORIZONTAL LAYOUT (Fourier along lines)
  nd_fourier1D_V(data2D, 2, N, 2*N  , 2*1 ,1 );
  // nd_fourier1D (data2D, 2, N, 2*N  , 2*1, 1);
  for( i=0; i<N*2; i++) {
    printf("%f ", data2D[i]);
  }
  printf("\n");
  for( i=0; i<N*2; i++) {
    printf("%f ", data2D[2*N+i]);
  }
  printf("\n");
  
  // TESTING vertical LAYOUT (Fourier along columns)
  nd_fourier1D_V(data2D_V, 2, N,   2  , 4, 1);
  // nd_fourier1D(data2D_V, 2, N,   2  , 4, 1);
  for ( k=0; k<N; k++) {
    for( i=0; i<4; i++) {
      printf("%f ", data2D_V[k*4+i]);
    }
    printf("\n");
  }

}
